package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"

	"github.com/fasthttp/websocket"
)

var symbols = []string{
	"BTC", "ETH", "ADA", "XRP", "BCH", "LTC", "EOS", "BSV", "TRX", "XLM",
	"NEO", "MIOTA", "ICX", "KNC", "REP", "OMG", "ZIL", "BNT", "WAN", "NANO",
	"VIB", "ENJ", "USDT", "TUSD", "PAX", "ZUSD", "DAI", "WBTC", "WETH", "WBCH",
	"RBTC", "RETH", "RBCH", "WLTC", "WXRP", "WADA", "WTRX", "WXLM", "WNEO", "WMIOTA",
	"WICX", "WKNC", "WREP", "WOMG", "WZIL", "WBNT", "WAN", "WNANO", "WVIB", "WENJ",
	"WUSDT", "WTUSD", "WPAX", "WZUSD", "WDAI",
}

var newsTitles = []string{
	"$SYMBOL_3 and $SYMBOL_1 Soars to New All-Time High Amidst Bullish Market Trends",
	"$SYMBOL_1 Announces Partnership With Major Financial Institution",
	"Is $SYMBOL_1, $SYMBOL_2 and $SYMBOL_3 are the Next Revolutionary Blockchain Technology?",
	"Is ($SYMBOL_2,$SYMBOL_1) are the Next Revolutionary Blockchain Technology?",
	"Investors Rush to Buy $SYMBOL_1 Amidst Positive Market Sentiment",
	"$SYMBOL_3 Implements Innovative Blockchain Solution for Supply Chain Management",
	"[$SYMBOL_2] Partners With Global Payment Processor for Seamless Transactions",
	"Will $SYMBOL_3 Lead the Next Bull Run in the Cryptocurrency Market or $SYMBOL_1?",
	"$SYMBOL_1 with $SYMBOL_2 Unveils Groundbreaking Proof-of-Stake Consensus Algorithm",
	"Investors Flock to Buy ($SYMBOL_2, $SYMBOL_1) Amidst Growing Institutional Interest",
	"$SYMBOL_3 Joins Forces With Major E-Commerce Platform for Crypto Payments",
	"Is $SYMBOL_2 Set to Disrupt the Gaming Industry with Blockchain Technology?",
	"$SYMBOL_3 Sees Surge in Trading Volume as Crypto Market Heats Up",
	"$SYMBOL_1 Launches Innovative DApp for Decentralized Governance",
	"$SYMBOL_2 Aims to Solve Scalability Issues With Upcoming Mainnet Upgrade",
	"Launches Decentralized Exchange for $SYMBOL_1, $SYMBOL_2 and $SYMBOL_3 Seamless Token Swaps",
	"Binance Announces Mainnet Upgrade for ($SYMBOL_2,$SYMBOL_1) Enhanced Functionality and Security",
	"$SYMBOL_3 Sets New Record for Daily Trading Volume",
}

func randomSymbol() string {
	idx := rand.Intn(len(symbols))

	return symbols[idx]
}

func randomNewsTitle() string {
	idx := rand.Intn(len(newsTitles))
	title := newsTitles[idx]

	for i := 1; i <= 3; i++ {
		pattern := fmt.Sprintf("$SYMBOL_%d", i)
		symbol := randomSymbol()

		title = strings.Replace(title, pattern, symbol, -1)
	}

	return title
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func handleConnection(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}
	defer conn.Close()

	for {
		_, _, err := conn.ReadMessage()
		//fmt.Println("Got a message", string(m), err)
		if err != nil {
			//log.Println(err)
			return
		}

		title := randomNewsTitle()
		err = conn.WriteMessage(websocket.TextMessage, []byte(title))
		//fmt.Println("Sent a message", title)
		if err != nil {
			log.Println(err)
			return
		}
	}
}

func main() {
	http.HandleFunc("/ws", handleConnection)
	fmt.Println("Starting server on :8888")
	err := http.ListenAndServe(":8888", nil)
	if err != nil {
		log.Fatal(err)
	}
}
