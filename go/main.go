package main

import (
	"fmt"
	"log"
	"regexp"

	"github.com/fasthttp/websocket"
)

func WebSocketConnect() (*websocket.Conn, error) {
	c, _, err := websocket.DefaultDialer.Dial("ws://localhost:8888/ws", nil)
	if err != nil {
		log.Fatal("dial:", err)
	}

	return c, err
}

func WebsocketGetResponse(c *websocket.Conn) ([]byte, error) {
	if err := c.WriteMessage(websocket.TextMessage, []byte("hello")); err != nil {
		return nil, err
	}

	_, m, err := c.ReadMessage()

	return m, err
}

var SymbolRegExp = regexp.MustCompile(`\b[A-Z]{2,5}\b`)

func ExtractSymbolsRegExp(title string) []string {
	return SymbolRegExp.FindAllString(title, -1)
}

func main() {
	c, err := WebSocketConnect()
	if err != nil {
		panic(err)
	}

	titles := make([]string, 0)

	for i := 0; i < 1000; i++ {
		title, err := WebsocketGetResponse(c)
		if err != nil {
			panic(err)
		}

		titles = append(titles, string(title))
	}

	fmt.Printf("%q", titles)
}
