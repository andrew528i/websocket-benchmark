use std::time::Duration;

use criterion::{black_box, Criterion, criterion_group, criterion_main};
use rand::Rng;

pub fn criterion_benchmark(c: &mut Criterion) {
    let mut rng = rand::thread_rng();
    let mut group = c.benchmark_group("sample-size-example");

    group.sample_size(1000).warm_up_time(Duration::from_millis(1));

    group.bench_function("extract_symbols_re", |b| b.iter(|| {
        let title = benchmark::constants::TITLES[rng.gen_range(0..benchmark::constants::TITLES.len())];

        benchmark::extract_symbols_re(title);
    }));

    group.bench_function("tungstenite_connect", |b| b.iter(|| {
        let mut socket = benchmark::tungstenite_connect();

        black_box(socket.close(None)).unwrap();
    }));

    let mut socket = black_box(benchmark::tungstenite_connect());

    group.bench_function("tungstenite_get_response", |b| b.iter(|| {

        benchmark::tungstenite_get_response(&mut socket);

    }));

    black_box(socket.close(None)).unwrap();

    let rt = tokio::runtime::Runtime::new().unwrap();

    group.bench_function("tokio_tungstenite_connect", |b| b.to_async(&rt).iter(|| {
        benchmark::tokio_tungstenite_connect()
    }));

    group.bench_function("websocket_connect", |b| b.iter(|| {
        let socket = benchmark::websocket_connect();

        black_box(socket.shutdown()).unwrap();
    }));

    let mut ws = black_box(benchmark::websocket_connect());

    group.bench_function("websocket_get_response", |b| b.iter(|| {

        benchmark::websocket_get_response(&mut ws);

    }));

    black_box(ws.shutdown()).unwrap();

    group.finish();
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
