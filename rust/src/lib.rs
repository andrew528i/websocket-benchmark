use std::net::TcpStream;
use tokio_tungstenite::{connect_async, WebSocketStream};

use tungstenite::client::connect;
use tungstenite::Message;
use tungstenite::protocol::WebSocket;
use tungstenite::stream::MaybeTlsStream;
use websocket::{ClientBuilder, OwnedMessage};
use websocket::sync::Client;

pub mod constants;

#[inline]
pub fn extract_symbols_re(title: &str) -> Vec<&str> {
    constants::SYMBOL_PATTERN.find_iter(title)
        .map(|m| m.as_str())
        .collect()
}

#[inline]
pub fn tungstenite_connect() -> WebSocket<MaybeTlsStream<TcpStream>> {
    let url = "ws://localhost:8888/ws";

    let (socket, _) = connect(url).unwrap();

    socket
}

#[inline]
pub fn tungstenite_get_response(socket: &mut WebSocket<MaybeTlsStream<TcpStream>>) -> String {
    socket.send(Message::Text("hello".to_string())).unwrap();

    let response = socket.read().unwrap();

    return match response {
        Message::Text(msg_text) => { msg_text },
        _ => "".to_string(),
    }
}

#[inline]
pub async fn tokio_tungstenite_connect() -> WebSocketStream<tokio_tungstenite::MaybeTlsStream<tokio::net::TcpStream>> {
    let url = "ws://localhost:8888/ws";

    let (ws_stream, _) = connect_async(url).await.expect("Failed to connect");

    ws_stream
}

#[inline]
pub fn websocket_connect() -> Client<TcpStream> {
    let client = ClientBuilder::new("ws://localhost:8888/ws")
        .unwrap()
        .add_protocol("rust-websocket")
        .connect_insecure()
        .unwrap();

    client
}

#[inline]
pub fn websocket_get_response(client: &mut Client<TcpStream>) -> String {
    client.send_message(&websocket::Message::text("hello")).unwrap();

    let resp = client.recv_message().unwrap();

    match resp {
        OwnedMessage::Text(t) => t,
        _ => "".to_string(),
    }
}

