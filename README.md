### 1. Run WebSocket server

```bash
$ cd websocket-server/
$ go run ./...
Starting server on :8888
```

### 2. Run Python benchmarks

```bash
# pip3 install pipenv
$ cd python
$ pipenv install
$ pipenv shell
$ python main.py
websockets_connect.................661980 ns
websockets_get_response............81928 ns
websocket_connect..................405725 ns
websocket_get_response.............45892 ns
extract_symbols_re.................967 ns
```

### 3. Run Go benchmarks

```bash
$ cd go
$ go test -bench=. -count=1
goos: darwin
goarch: arm64
pkg: benchmark
BenchmarkWebSocketConnect-10        	    5421	    222221 ns/op
BenchmarkWebsocketGetResponse-10    	   40357	     26428 ns/op
BenchmarkExtractSymbolsRegExp-10    	  950472	      1249 ns/op
PASS
ok  	benchmark	4.942s
```

### 4. Run Rust benchmarks

```bash
$ cd rust
$ cargo bench
Compiling benchmark v0.1.0 (/Users/master/Workspace/kbu-research/benchmark-websocket-parsing/rust)
    Finished bench [optimized] target(s) in 1.45s
warning: the following packages contain code that will be rejected by a future version of Rust: traitobject v0.1.0
note: to see what the problems were, use the option `--future-incompat-report`, or run `cargo report future-incompatibilities --id 1`
     Running unittests src/lib.rs (target/release/deps/benchmark-01ea8591ca07b42e)

running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s

     Running benches/benchmark.rs (target/release/deps/benchmark-597ea4c5ed20cbf4)
Gnuplot not found, using plotters backend
sample-size-example/extract_symbols_re
                        time:   [206.30 ns 206.55 ns 206.82 ns]
                        change: [+0.1656% +0.3312% +0.4805%] (p = 0.00 < 0.05)
                        Change within noise threshold.
Found 108 outliers among 1000 measurements (10.80%)
  1 (0.10%) low mild
  32 (3.20%) high mild
  75 (7.50%) high severe
sample-size-example/tungstenite_connect
                        time:   [197.36 µs 205.19 µs 214.69 µs]
                        change: [-5.9186% +0.5231% +6.9083%] (p = 0.88 > 0.05)
                        No change in performance detected.
Found 103 outliers among 1000 measurements (10.30%)
  1 (0.10%) low mild
  21 (2.10%) high mild
  81 (8.10%) high severe
sample-size-example/tungstenite_get_response
                        time:   [25.200 µs 25.354 µs 25.535 µs]
                        change: [-1.1748% -0.2985% +0.6075%] (p = 0.53 > 0.05)
                        No change in performance detected.
Found 56 outliers among 1000 measurements (5.60%)
  10 (1.00%) low severe
  1 (0.10%) low mild
  19 (1.90%) high mild
  26 (2.60%) high severe
sample-size-example/tokio_tungstenite_connect
                        time:   [205.89 µs 207.69 µs 209.68 µs]
                        change: [-7.9958% -3.1297% +0.0093%] (p = 0.17 > 0.05)
                        No change in performance detected.
Found 53 outliers among 1000 measurements (5.30%)
  8 (0.80%) low severe
  5 (0.50%) low mild
  14 (1.40%) high mild
  26 (2.60%) high severe
sample-size-example/websocket_connect
                        time:   [178.99 µs 180.86 µs 182.95 µs]
                        change: [-2.7026% -1.0373% +0.6449%] (p = 0.23 > 0.05)
                        No change in performance detected.
Found 79 outliers among 1000 measurements (7.90%)
  10 (1.00%) low severe
  1 (0.10%) low mild
  21 (2.10%) high mild
  47 (4.70%) high severe
sample-size-example/websocket_get_response
                        time:   [25.606 µs 25.795 µs 26.026 µs]
                        change: [-0.2121% +0.8081% +1.8568%] (p = 0.14 > 0.05)
                        No change in performance detected.
Found 53 outliers among 1000 measurements (5.30%)
  2 (0.20%) low mild
  28 (2.80%) high mild
  23 (2.30%) high severe
```

### 5. Overall results


|        | WebSocket connect                                                          | WebSocket read and write                   | Extract Symbols RegExp |
|--------|----------------------------------------------------------------------------|--------------------------------------------|------------------------|
| Python | websockets: 684733 ns, websocket: 426104 ns                                | websockets: 82590 ns websocket: 46518 ns   | 947 ns                 |
| Go     | 265144 ns                                                                  | 26001 ns                                   | 1245 ns                |
| Rust   | tungstenite: 182600 ns, tokio_tungstenite: 203110 ns, websocket: 185880 ns | tungstenite: 25625 ns, websocket: 25431 ns | 206 ns                 |